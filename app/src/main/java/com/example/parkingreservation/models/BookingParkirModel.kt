package com.example.parkingreservation.models

data class BookingParkirModel(
    val jamKeluar: String = "",
    val jamMasuk: String = "",
    val kodeBooking: String = "",
    val nomorTempat: String = ""
)
