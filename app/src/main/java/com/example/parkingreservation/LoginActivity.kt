package com.example.parkingreservation

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.example.parkingreservation.databinding.ActivityLoginBinding
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var uiBinding: ActivityLoginBinding

    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var buttonLogin: Button

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uiBinding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        initialComponents()
        buttonLogin.setOnClickListener {
            login(etEmail.text.toString(), etPassword.text.toString())
        }
    }

    private fun login(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
            if (it.isSuccessful) {
                // login berhasil
                // finish()
                val intent = Intent(this, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

                startActivity(intent)
            } else {
                // login gagal
                Log.d(TAG, "login: gagal")
            }
        }
    }

    private fun initialComponents() {
        etEmail = uiBinding.editTextEmail
        etPassword = uiBinding.editTextPassword
        buttonLogin = uiBinding.btnLogin

        auth = FirebaseAuth.getInstance()
    }
}