package com.example.parkingreservation

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.databinding.DataBindingUtil
import com.example.parkingreservation.databinding.ActivityHomeBinding
import com.example.parkingreservation.models.BookingParkirModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class HomeActivity : AppCompatActivity() {

    private lateinit var uiBinding: ActivityHomeBinding
    private lateinit var buttonLogout: Button
    private lateinit var buttonAddBooking: Button

    private lateinit var auth: FirebaseAuth

    private lateinit var database: DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        uiBinding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        initialComponents()
        buttonLogout.setOnClickListener {
            logout()
        }

        buttonAddBooking.setOnClickListener {
            saveBookingParkir()
        }

        readFromDB()
    }

    private fun logout() {
        auth.signOut()

        val intent = Intent(this, MainActivity::class.java)
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        startActivity(intent)
    }

    private fun initialComponents() {
        buttonLogout = uiBinding.btnLogout
        buttonAddBooking = uiBinding.buttonAddBooking
        database =
            Firebase.database("https://parkingapp-c1996-default-rtdb.asia-southeast1.firebasedatabase.app").reference

        auth = FirebaseAuth.getInstance()
    }

    private fun readFromDB() {
        database.child("status_parkir_tbl").child("6BxpEdXcXU").child("nomor_tempat").get()
            .addOnSuccessListener {
                Log.i(TAG, "readFromDB: ${it.value}")
            }
    }

    private fun saveBookingParkir() {
        val BookingParkirModel = BookingParkirModel("18:00", "09:00", "2011DBH", "2")

        database.child("booking_parkir_tbl").child("TUFGAMING").setValue(BookingParkirModel).addOnCompleteListener {
            if (it.isSuccessful) {
                Log.d(TAG, "saveBookingParkir: Success")
            } else {
                Log.d(TAG, "saveBookingParkir: ${it.exception}")
            }
        }

    }
}